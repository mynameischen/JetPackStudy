package com.exam.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 *  author  : ctb
 *  date    : 2022/11/2 10:41
 *  desc    : ViewModel一般与LiveData配合使用
 */
class MainActivityViewModel: ViewModel() {
    private var i = 0
    private val testNum = MutableLiveData<Int>()

    fun getTestNum(): MutableLiveData<Int>{
        return testNum
    }

    fun addOne(){
        i++
        testNum.value = i
    }

}