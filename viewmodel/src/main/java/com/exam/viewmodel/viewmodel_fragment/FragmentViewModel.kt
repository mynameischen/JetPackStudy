package com.exam.viewmodel.viewmodel_fragment

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 *  author  : ctb
 *  date    : 2022/11/2 11:24
 *  desc    :
 */
class FragmentViewModel: ViewModel() {
    private val testLiveData = MutableLiveData<Int>()
    private var i = 0

    fun getLiveData(): MutableLiveData<Int> {
        return testLiveData
    }

    fun addOne(){
        i++
        testLiveData.value = i
    }
}