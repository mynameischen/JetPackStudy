package com.exam.viewmodel.viewmodel_fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.exam.viewmodel.databinding.FragmentLeftBinding


class LeftFragment : Fragment() {

    private val viewModel: FragmentViewModel by activityViewModels()
    private lateinit var binding: FragmentLeftBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLeftBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.let { viewModel.getLiveData().observe(it){
                binding.tvLeft.text = it.toString()
            }
        }

    }

}