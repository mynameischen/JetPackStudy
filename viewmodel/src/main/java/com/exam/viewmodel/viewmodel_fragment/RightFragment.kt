package com.exam.viewmodel.viewmodel_fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.exam.viewmodel.R
import com.exam.viewmodel.databinding.FragmentLeftBinding
import com.exam.viewmodel.databinding.FragmentRightBinding

class RightFragment : Fragment() {
    private val viewModel: FragmentViewModel by activityViewModels()
    private lateinit var binding: FragmentRightBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRightBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let { viewModel.getLiveData().observe(it){
                binding.tvRight.text = it.toString()
            }
        }
    }
}