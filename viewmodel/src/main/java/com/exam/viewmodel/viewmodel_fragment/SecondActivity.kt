package com.exam.viewmodel.viewmodel_fragment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.exam.viewmodel.R
import com.exam.viewmodel.databinding.ActivitySecondBinding

class SecondActivity : AppCompatActivity() {

    private val viewModel: FragmentViewModel by viewModels()
    private lateinit var binding: ActivitySecondBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySecondBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnAdd.setOnClickListener{
            viewModel.addOne()
        }
    }
}