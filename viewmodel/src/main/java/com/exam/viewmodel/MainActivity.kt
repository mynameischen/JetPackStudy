package com.exam.viewmodel

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.exam.viewmodel.databinding.ActivityMainBinding
import com.exam.viewmodel.viewmodel_fragment.SecondActivity

class MainActivity : AppCompatActivity() {

    private val viewModel: MainActivityViewModel by viewModels()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // 使用viewmodel
        viewModel.getTestNum().observe(this){
            binding.tvText.text = it.toString()
        }

        click()
    }

    private fun click(){
        binding.btnAdd.setOnClickListener {
            viewModel.addOne()
        }

        binding.btnGoToSecondActivity.setOnClickListener {
            val intent: Intent = Intent(this, SecondActivity::class.java)
            startActivity(intent)
        }
    }


}