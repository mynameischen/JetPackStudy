package com.jetpackstudy

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.activity.viewModels
import androidx.lifecycle.ViewModel
import com.jetpackstudy.databinding.ActivityMainBinding
import com.jetpackstudy.lifecycle.MyDefaultLifecycleObserver
import com.jetpackstudy.viewmodel.MainActivityViewModel

class MainActivity : AppCompatActivity() {

    private val viewModel: MainActivityViewModel by viewModels()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // 观察生命周期
        lifecycle.addObserver(MyDefaultLifecycleObserver())

        // 使用viewmodel
        viewModel.getTestNum().observe(this){
            binding.tvText.text = it.toString()
        }

        click()
    }

    private fun click(){
        binding.btnAdd.setOnClickListener {
            viewModel.addOne()
        }
    }


}